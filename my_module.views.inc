<?php

/**
 * @file
 * Create the custom Views field that gets a value programmatically.
 */

/**
 * Implements hook_views_data_alter().
 */
function my_module_views_data_alter(array &$data) {

  $data['node_field_data']['custom_field'] = [
    'title' => t('Custom field'),
    'field' => [
      'title' => t('Custom field'),
      'help' => t('Custom field that gets a value programmatically using a custom module.'),
      'id' => 'custom_field',
    ],
  ];
}
