<?php

namespace Drupal\my_module\Plugin\views\field;

/**
 * @file
 * Definition of custom field.
 */

use Drupal\views\Plugin\views\field\FieldPluginBase;
use Drupal\views\ResultRow;
use Drupal\Core\Url;

/**
 * Field handler to flag the node type.
 *
 * @ingroup views_field_handlers
 *
 * @ViewsField("custom_field")
 */
class CustomField extends FieldPluginBase {

  /**
   * Query.
   */
  public function query() {
    // Leave empty to avoid a query on this field.
  }

  /**
   * Define the available options.
   */
  protected function defineOptions() {
    $options = parent::defineOptions();
    return $options;
  }

  /**
   * Render.
   */
  public function render(ResultRow $values) {

    $title = $values->_entity->getTitle();
    $url = Url::fromRoute('entity.node.canonical', ['node' => $values->nid]);
    $link = [
      '#type' => 'link',
      '#url' => $url,
      '#title' => $title,
    ];

    return $link;
  }

}
